<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : JP
 *  License    : Protected 
 *  Email      : jaka.pondang@gmail.com 
 *  Website    : www.jakapondang.com
 *  ======================================= 
 */

$config['paths'] = array(
    'logs' => 'var/logs',
    'themes' => 'assets/themes/orb/',
);
