<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : JP
 *  License    : Protected 
 *  Email      : jaka.pondang@gmail.com 
 *   
 *  ======================================= 
 */
class Gear{
    var $CI = null;

    function Gear(){
        $this->CI =& get_instance();
        $this->CI->load->library(array('session','user_agent','parser'));
        $this->CI->load->helper('url');
//        $this->CI->load->model(array('gear_model'));
        $this->classes = strtolower(__CLASS__);

    }

    public function logs($filename="",$message=""){


        if(!empty($filename)&& !empty($message)){

            $path = $this->CI->config->item('paths');

            $folder = explode("/",$path['logs']);

            if(!file_exists($folder[0])){
                mkdir( $folder[0], 0777);
            }
            if(!file_exists($folder[0]."/".$folder[1])){
                mkdir($folder[0]."/".$folder[1], 0777);
            }
            $filename = $path['logs']."/".$filename.".log";
            $files = file_exists($filename);

            if($files==TRUE ){
                $current = file_get_contents($filename);
                $current .= $message."\n";
                file_put_contents($filename, $current);

            }else{
                $log_file = fopen($filename, "w") or die("Unable to open file!");
                $txt = $message."\n";
                fwrite($log_file, $txt);
                fclose($log_file);
            }
        }
    }

    public function links(){
        $result = "";
        $path= $this->CI->config->item('paths');
        $themes_link=$path['themes'];
        $result['themesurl'] = base_url().$themes_link;
        $result['baseurl'] = base_url();

        return $result;
    }



}

?>