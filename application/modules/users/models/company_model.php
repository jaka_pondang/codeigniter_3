<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : JP
 *  License    : Protected 
 *  Email      : jaka.pondang@gmail.com 
 *   
 *  ======================================= 
 */
class Company_model extends CI_Model {
	
	var $company_table = 'aa_company';
	
    function __construct() {
        parent::__construct();
        $this->load->database();
        
    }
    
    function listCompany(){
    	$query = $this->db->get_where($this->company_table, array('is_deleted' => 'n','status'=>1));
		// $query = $this->db->get($this->company_table);
		return $query->result();
	}
	
}	