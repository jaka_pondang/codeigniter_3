<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : JP
 *  License    : Protected 
 *  Email      : jaka.pondang@gmail.com 
 *   
 *  ======================================= 
 */
class Userl_model extends CI_Model {
    var $user_core  = 'user_core';
	var $user_extra = 'user_extra';

    public function __construct() {
        parent::__construct();
        $this->load->database();
        
    }

    public function check_users_exits($username){
        $sql = "SELECT * FROM {$this->user_core}
        LEFT JOIN {$this->user_extra} ON {$this->user_core}.user_id={$this->user_extra}.user_id
    	WHERE
    	( {$this->user_core}.user_login_id='{$username}' OR {$this->user_core}.user_email='{$username}')
        AND {$this->user_core}.user_status=1 AND {$this->user_core}.is_deleted='n' ";


        return $this->db->query($sql)->num_rows();
    }

    public function insert_user_core($data){
        $this->db->set($data);
        $this->db->insert($this->user_core);

        if($this->db->affected_rows() == 0){//There was a problem!
            return false;
        }else{
            return  true;
        }
    }

    public function insert_user_extra($data){
        $this->db->set($data);
        $this->db->insert($this->user_extra);

        if($this->db->affected_rows() == 0){//There was a problem!
            return false;
        }else{
            return  true;
        }
    }







    function authCheck($username,$company)
    {
    	$sql = "select * from {$this->user_extra} 
    	left join {$this->user_info} 
    	on {$this->user_extra}.user_id={$this->user_info}.user_id 
    	where 
    	{$this->user_info}.user_login_id='{$username}' 
    	and
    	{$this->user_info}.company_id='{$company}'
        and
        {$this->user_info}.is_deleted='n'
        and
        {$this->user_info}.user_status=1
    	";
    	return $this->db->query($sql)->num_rows();
    }
    
    function checkAdmin($data){
    //Check against user table
		$this->db->where($data); 
		$query = $this->db->get_where($this->user_info);
		
		return $query->num_rows(); //user_admin already exists
	}
	function getUpdateAdmin($data){
    //Check against user table
    	
		$this->db->where($data); 
		$query = $this->db->get_where($this->user_info);
		
		return $query; //user_admin already exists
	}

	function getAdminInfo($username)
    {
    	$sql = "select * from {$this->user_info} 
        left join {$this->user_extra} 
        on {$this->user_info}.user_id={$this->user_extra}.user_id 
        left join {$this->user_table_meta} 
        on {$this->user_info}.user_id={$this->user_table_meta}.user_id
    	where 
    	{$this->user_info}.user_login_id='{$username}'
        and 
        {$this->user_info}.is_deleted='n' 
    	";
    	// return $this->db->query($sql);
        return $this->db->query($sql);
    }

	 function getAdmin($data){
    
		$this->db->where($data); 
		$query = $this->db->get_where($this->user_table);
		
		return $query->row_array(); //get array
	}
	
	
    function insertAdminInfo($data){
    	$this->db->set($data);
        $this->db->insert($this->user_core);
        $insert_id = $this->db->insert_id();

        if($this->db->affected_rows() == 0){//There was a problem!
			return false;
		}else{
			return  $insert_id;
		}		
    }

    function insertAdminExtra($data,$user_id){
        $this->db->set($data);
        $this->db->insert($this->user_extra);

        if($this->db->affected_rows() == 0){//There was a problem!
            return false;
        }else{
            return  true;
        }       
    }

    function insertAdminMeta($data,$user_id){
        $this->db->set($data);
        $this->db->insert($this->user_table_meta);
       /* echo $this->db->last_query();
        die();
*/
        if($this->db->affected_rows() == 0){//There was a problem!
            return false;
        }else{
            return  true;
        }       
    }
    
    function updateAdminInfo($where , $data ){
    	$this->db->where($where);
		$this->db->update($this->user_info, $data); 
        // error_log($this->db->last_query());
        // error_log($this->db->affected_rows());
    	if($this->db->affected_rows() == 0){//There was a problem!
			return false;
		} else {
            return true;
        }
    }

    function updateAdminExtra($where , $data ){
        $this->db->where($where);
        $this->db->update($this->user_extra, $data); 
        if($this->db->affected_rows() == 0){//There was a problem!
            return false;
        }
    }

    function updateAdminMeta($where , $data ){
        $this->db->where($where);
        $this->db->update($this->user_table_meta, $data);
        // error_log($this->db->last_query());
        // error_log($this->db->affected_rows()); 
        if($this->db->affected_rows() == 0){//There was a problem!
            return false;
        } else {
            return true;
        }
    }
}	