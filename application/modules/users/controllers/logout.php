<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : JP
 *  License    : Protected 
 *  Email      : jaka.pondang@gmail.com 
 *   
 *  ======================================= 
 */
class Logout extends MX_Controller{

	var $CI = null;

	function __construct(){
		parent::__construct();
		$this->load->library(array('userl'));
		//$this->general->checkSession(__CLASS__);
	}
	public function index()
	{
		//users log
		/*echo '<pre>';
		print_r($this->session->userdata);
		echo '</pre>';
		die();*/
		/*echo '<pre>';
		print_r($this->session->userdata);
		echo '</pre>';
		echo $this->session->userdata['result_object'][0]->user_id;*/
		$user_id =$this->session->userdata('user_id');
		if(isset($user_id) && !empty($user_id))
		{
			$this->general->insert_users_log($user_id,"USER ARE LOGOUT");
		}
		$this->userl->logout();

		/*echo '<pre>';
		print_r($this->session->userdata);
		echo '</pre>';*/
		// die();
		redirect('users/login');
	}
	
	
	public function debugs()
	{
		
		$this->userl->create("superadmin", "admin007", "jakapondang@gmail.com",1);
		print 2;
		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */