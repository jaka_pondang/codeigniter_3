
CREATE TABLE `user_core` (
`user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`user_email` varchar(255) NOT NULL,
`user_login_id` varchar(255) NOT NULL DEFAULT '',
`user_pass` varchar(60) NOT NULL DEFAULT '',
`user_status` int(1) NOT NULL DEFAULT '1',
`author` int(11) unsigned NOT NULL DEFAULT '0',
`created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Creation date',
`modify` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`is_deleted` enum('n','y') NOT NULL DEFAULT 'n',
PRIMARY KEY (`user_id`),
UNIQUE KEY `idxu_user_login_id` (`user_login_id`,`is_deleted`) USING BTREE,
UNIQUE KEY `idxu_user_email` (`user_email`,`is_deleted`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE `user_extra` (
`user_id` int(11) unsigned NOT NULL,
`user_formal_name` varchar(255) DEFAULT NULL,
`user_last_login` datetime DEFAULT NULL,
`author` int(11) unsigned NOT NULL DEFAULT '0',
`created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Creation date',
`modify` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
UNIQUE KEY `idxu_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;