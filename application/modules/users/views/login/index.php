<body>
<div class="colorful-page-wrapper">
  <div class="center-block">
    <div class="login-block">
      <form action="{baseurl}users/login/action" method="post" id="login-form" class="orb-form">
        <header>
          <div class="image-block"><img src="{themesurl}images/BIGdutyfree.jpg" alt="User" /></div>
          Login to BIG BACK <!--<small>Have no account? &#8212; <a href="#">Register</a></small>-->
          
          </header>

        <fieldset>
          <section>
            <div class="row">
              <label class="label col col-4">Company</label>
              <div class="col col-8">
                <label class="select"> <i class="icon-append fa fa-user"></i>
                  <select name="company">

                  </select>
                </label>
              </div>
            </div>
          </section>
          <section>
         
            <div class="row">
              <label class="label col col-4">Username</label>
              <div class="col col-8">
                <label class="input"> <i class="icon-append fa fa-user"></i>
                  <input name="username"  type="text">
                </label>
              </div>
            </div>
          </section>
          <section>
            <div class="row">
              <label class="label col col-4">Password</label>
              <div class="col col-8">
                <label class="input"> <i class="icon-append fa fa-lock"></i>
                  <input type="password" name="password">
                </label>
                <!--<div class="note"><a href="#">Forgot password?</a></div>-->
              </div>
            </div>
          </section>
          <section>
            <div class="row">
              <div class="col col-4"></div>
              <div class="col col-8">
                <label class="checkbox">
                  <input type="checkbox" name="remember" checked>
                  <i></i>Keep me logged in</label>
              </div>
            </div>
          </section>
        </fieldset>
        <footer>
          <button type="submit" class="btn btn-default">Log in</button>
        </footer>
      </form>
    </div>
    
    <div class="copyrights"> Big Duty Free
       &copy; 2015 </div>
  </div>
</div>

<!--Main App--> 
<script type="text/javascript">

</script>



<!--/Scripts-->

</body>
</html>