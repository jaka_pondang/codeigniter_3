<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>GROUD OPS | Login</title>
<link href="{themesurl}css/styles.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<script type="text/javascript" src="{themesurl}js/vendors/horisontal/modernizr.custom.js"></script>
    <!--Scripts-->
    <!--JQuery-->
    <script type="text/javascript" src="{themesurl}js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="{themesurl}js/vendors/jquery/jquery-ui.min.js"></script>

    <!--Forms-->
    <script type="text/javascript" src="{themesurl}js/vendors/forms/jquery.form.min.js"></script>
    <script type="text/javascript" src="{themesurl}js/vendors/forms/jquery.validate.min.js"></script>
</head>

