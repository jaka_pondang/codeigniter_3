<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : JP
 *  License    : Protected 
 *  Email      : jaka.pondang@gmail.com 
 *   
 *  ======================================= 
 */
require_once('phpass-0.3/PasswordHash.php');

define('PHPASS_HASH_STRENGTH', 8);
define('PHPASS_HASH_PORTABLE', false);


/**
 * SimpleLoginSecure Class
 *
 * Makes authentication simple and secure.
 *
 * Simplelogin expects the following database setup. If you are not using 
 * this setup you may need to do some tweaking.
 *   
 * 
 *   CREATE TABLE `users` (
 *     `user_id` int(10) unsigned NOT NULL auto_increment,
 *     `user_admin` varchar(255) NOT NULL default '',
 *     `user_pass` varchar(60) NOT NULL default '',
 *     `user_date` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'Creation date',
 *     `user_modified` datetime NOT NULL default '0000-00-00 00:00:00',
 *     `user_last_login` datetime NULL default NULL,
 *     PRIMARY KEY  (`user_id`),
 *     UNIQUE KEY `user_admin` (`user_admin`),
 *   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 * 
 * @package   SimpleLoginSecure
 * @version   2.1.1
 * @author    Stéphane Bourzeix, Pixelmio <stephane[at]bourzeix.com>
 * @copyright Copyright (c) 2012-2013, Stéphane Bourzeix
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt
 * @link      https://github.com/DaBourz/SimpleLoginSecure
 */
class Userl
{
	var $CI;
	


	/**
	 * Create a user account
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
    public function __construct(){

        $this->CI =&get_instance();
       	$this->CI->load->models(array('userl_model'));
        
	}

    public function create($user_name = '', $user_pass = '', $user_email='',$user_status="", $user_formal_name="",$auto_login = false)
	{

		//Make sure account info was sent
		if($user_name == '' OR $user_pass == '') {
			return false;
		}

		$count = $this->CI->userl_model->check_users_exits($user_name);
		if($count >0){
			return false;
		}

		//Hash user_pass using phpass
		$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
		$user_pass_hashed = $hasher->HashPassword($user_pass);
	
		//Insert account into the database
		$data = array(
					'user_login_id' => $user_name,
					'user_email' => $user_email,
                    'user_pass' => $user_pass_hashed,
					'user_status' => $user_status,
					'author' => 1 ,//$this->CI->session->userdata('user_id')
					'created' => date('c'),
					'modify' => date('c')
				);

		$user_id = $this->CI->userl_model->insert_user_core($data);
        if($user_id>0){
            $data = array(
                'user_id' => $user_id,
                'user_formal_name' => $user_formal_name,
                'author' =>1 ,//$this->CI->session->userdata('user_id')
                'created' => date('c'),
                'modify' => date('c')
            );

            if($auto_login){
                $data['user_last_login' ] = date('c');
            }

            $insert_id = $this->CI->userl_model->insert_user_extra($data);

            if($auto_login){
                $this->login($user_name, $user_pass);
            }

        }

        return $insert_id;
	}
	/**
	 * Login and sets session variables
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function login($user_name = '', $user_pass = '')
	{
		

		if($user_name == '' OR $user_pass == '')
			return false;

		
		//Check if already logged in
		if($this->CI->session->userdata('user_admin') == $user_name)
        return true;
		
		
		//Check against user table
		$userdata = array('user_admin'=> $user_name , 'user_status'=> "1" , );
		$queryCount = $this->CI->userl_model->checkAdmin($userdata);


		if ($queryCount > 0)
        {
			$user_data = $this->CI->userl_model->getAdmin($userdata); 

			$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);

			if(!$hasher->CheckPassword($user_pass, $user_data['user_pass']))
				return false;

			//Destroy old session
			$this->CI->session->sess_destroy();
			
			//Create a fresh, brand new session
			$this->CI->session->sess_create();
            //join user meta with user ;
            $updateData = array("user_last_login"=>date("Y-m-d H:i:s"),);
            $updateWhere = array("user_id"=>$user_data['user_id']);
			
         	$this->CI->userl_model->updateAdmin($updateWhere,$updateData);

			$this->CI->session->set_userdata($user_data);
            
			
			return true;
		} 
		else 
		{
			return false;
		}	

	}

	function newLogin($user_admin = '', $user_pass = '', $company = 0)
	{
		if($user_admin == '' OR $user_pass == '')
			return false;

		//Check against user table
		$userdata = array('user_admin'=> $user_admin , 'user_status'=> "1" , 'company' => $company);

		$queryCount = $this->CI->userl_model->authCheck($user_admin,$company);

		if ($queryCount > 0)
        {
			$user_data = $this->CI->userl_model->getAdminInfo($user_admin); 
			// print_r($user_data->result());
			// die();
			// print_r($user_data->result()[0]->user_pass);

			// print_r($user_data['user_pass']);
			$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);

			if(!$hasher->CheckPassword($user_pass, $user_data->result()[0]->user_pass))
				return false;

			//Destroy old session
			$this->CI->session->sess_destroy();
			
			//Create a fresh, brand new session
			$this->CI->session->sess_create();
            //join user meta with user ;
            $updateData = array("user_last_login"=>date("Y-m-d H:i:s"),);
            $updateWhere = array("user_id"=>$user_data->result()[0]->user_id);
			
         	$this->CI->userl_model->updateAdminExtra($updateWhere,$updateData);

			$this->CI->session->set_userdata($user_data->result()[0]);

			// $this->CI->session->set_userdata($user_data->result()[0]);

			/*echo '<pre>';
			print_r($this->CI->session->userdata);
			echo '</pre>';
			die();*/

			return true;
		} else 
		{
			return false;
		}
		
	}
	
	function login_return_role($user_admin = '', $user_pass = '',$user_role="") 
	{
		

		if($user_admin == '' OR $user_pass == '')
			return false;

		
		//Check if already logged in
		if($this->CI->session->userdata('user_admin') == $user_admin)
        return true;
		
		
		//Check against user table
		$userdata = array('user_admin'=> $user_admin , 'user_status'=> "1" , 'user_role'=>$user_role);
		$queryCount = $this->CI->userl_model->checkAdmin($userdata);


		if ($queryCount > 0)
        {
			$user_data = $this->CI->userl_model->getAdmin($userdata); 

			$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);

			if(!$hasher->CheckPassword($user_pass, $user_data['user_pass']))
				return false;

			//Destroy old session
			$this->CI->session->sess_destroy();
			
			//Create a fresh, brand new session
			$this->CI->session->sess_create();
            //join user meta with user ;
            $updateData = array("user_last_login"=>date("Y-m-d H:i:s"),);
            $updateWhere = array("user_id"=>$user_data['user_id']);
			
         	$this->CI->userl_model->updateAdmin($updateWhere,$updateData);

			$this->CI->session->set_userdata($user_data);
            
			
			return true;
		} 
		else 
		{
			return false;
		}	

	}
	
	/**
	 * Login VIA TABLETS
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function login_via_tablet($user_admin = '') 
	{
		if($user_admin == '' )
			return false;

		
		//Check if already logged in
		if($this->CI->session->userdata('user_admin') == $user_admin)
        return true;
		
		
		//Check against user table
		$userdata = array('user_admin'=> $user_admin , 'user_status'=> "1" , );
		$queryCount = $this->CI->userl_model->checkAdmin($userdata);


		if ($queryCount > 0)
        {
			$user_data = $this->CI->userl_model->getAdmin($userdata); 

			//Destroy old session
			$this->CI->session->sess_destroy();
			
			//Create a fresh, brand new session
			$this->CI->session->sess_create();
            //join user meta with user ;
            $updateData = array("user_last_login"=>date("Y-m-d H:i:s"),);
            $updateWhere = array("user_id"=>$user_data['user_id']);
			
         	$this->CI->userl_model->updateAdmin($updateWhere,$updateData);

			$this->CI->session->set_userdata($user_data);
            
			
			return true;
		} 
		else 
		{
			return false;
		}	

	}
	/**
	 * Logout user
	 *
	 * @access	public
	 * @return	void
	 */
	function logout() {
	

		$this->CI->session->sess_destroy();
		
	}

	/**
	 * Update a user account
	 *
	 * Only updates the email, just here for you can 
	 * extend / use it in your own class.
	 *
	 * @access	public
	 * @param integer
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function update($user_id = null, $user_admin = '',$user_formal_name = '',$user_email,$user_status,$user_role="",$user_pass = '', $auto_login = true) 
	{
		

		//Make sure account info was sent
		if($user_id == null OR $user_admin == '') {
			return false;
		}
		
		//Check against user table
		$dataUser = array('user_id'=> $user_id);
		$query = $this->CI->userl_model->getUpdateAdmin($dataUser);
		
		if ($query->num_rows() == 0){ // user don't exists
			return false;
		}
		
		//Update account into the database
		$data = array(
					'user_login_id' => $user_admin,
					'modify' => date('c'),
					'author' => $this->CI->session->userdata('user_id'),
					'user_email' => $user_email,
					'user_formal_name' => $user_formal_name,
					'user_status' => $user_status
				);
 
		
// die('121');
		$this->CI->userl_model->updateAdminInfo($dataUser, $data); //There was a problem! 
			// return false;						
		// die('122');
		//Update password into the database
		/*if($user_pass != '')
		{
			//Hash user_pass using phpass
			$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
			$user_pass_hashed = $hasher->HashPassword($user_pass);

			$data = array(
					'user_pass' => $user_pass_hashed,
					'author' => $this->CI->session->userdata('user_id'),
					'modify' => date('c')
				);
 
			if(!$this->CI->userl_model->updateAdminExtra($dataUser, $data)) //There was a problem! 
			return false;
		}*/

		//update role assign to user
		$data = array(
				'role_id' => $user_role,
				'author' => $this->CI->session->userdata('user_id'),
				'modify' => date('c')
			);

		$this->CI->userl_model->updateAdminMeta($dataUser, $data); //There was a problem! 
		// return false;

		if($auto_login){
			$user_data['user_admin'] = $user_admin;
			$user_data['user'] = $user_data['user_admin']; // for compatibility with Simplelogin
			
			$this->CI->session->set_userdata($user_data);
			}
		return true;
	}



	
	/**
	 * Delete user
	 *
	 * @access	public
	 * @param integer
	 * @return	bool
	 */
	function delete($user_id) 
	{
		$this->CI =& get_instance();
		
		if(!is_numeric($user_id))
			return false;			

		return $this->CI->db->delete($this->user_table, array('user_id' => $user_id));
	}
	
	
	/**
	* Edit a user password
	* @author    Stéphane Bourzeix, Pixelmio <stephane[at]bourzeix.com>
	* @author    Diego Castro <castroc.diego[at]gmail.com>
	*
	* @access  public
	* @param  string
	* @param  string
	* @param  string
	* @return  bool
	*/
	function edit_password($user_admin = '', $old_pass = '', $new_pass = '')
	{
		$this->CI =& get_instance();
		// Check if the password is the same as the old one
		$this->CI->db->select('user_pass');
		$query = $this->CI->db->get_where($this->user_table, array('user_admin' => $user_admin));
		$user_data = $query->row_array();

		$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);	
		if (!$hasher->CheckPassword($old_pass, $user_data['user_pass'])){ //old_pass is the same
			return FALSE;
		}
		
		// Hash new_pass using phpass
		$user_pass_hashed = $hasher->HashPassword($new_pass);
		// Insert new password into the database
		$data = array(
			'user_pass' => $user_pass_hashed,
			'modify' => date('c')
		);
		
		$this->CI->db->set($data);
		$this->CI->db->where('user_admin', $user_admin);
		if(!$this->CI->db->update($this->user_table, $data)){ // There was a problem!
			return FALSE;
		} else {
			return TRUE;
		}
	}

    function reset_password($user_id = '', $new_pass = '')
    {
        $this->CI =& get_instance();
       	$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
        // Hash new_pass using phpass
        $user_pass_hashed = $hasher->HashPassword($new_pass);

       /* echo $new_pass;
        echo '<br />';
        echo $user_pass_hashed;
        die();*/
        // Insert new password into the database
        $data = array(
            'user_pass' => $user_pass_hashed,
            'modify' => date('c')
        );

      
       	$where = array('user_id'=> $user_id);
        if(!$this->CI->userl_model->updateAdminExtra($where, $data)){ // There was a problem!
            return FALSE;
        } else {

            return TRUE;
        }
    }
    
    
  


}
?>
