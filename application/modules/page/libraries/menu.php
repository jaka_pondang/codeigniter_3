<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : JP
 *  License    : Protected 
 *  Email      : jaka.pondang@gmail.com 
 *   
 *  ======================================= 
 */

class Menu
{
	var $CI;
	
    function __construct(){

        $this->CI =&get_instance();
       	$this->CI->load->models(array('page/menu_model'));
       	$this->CI->load->library(array('parser'));
	}
	
	function main_menu(){
	
		
		$data = array(
					'blog_title' => 'My Blog Title',
					'blog_heading' => 'My Blog Heading'
					);

		$data = $this->CI->parser->parse('page/main_menu', $data);
		return $data ;
	}


}
?>
