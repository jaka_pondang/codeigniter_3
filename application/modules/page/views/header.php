<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="keywords" content="BIG BACKEND | <?php print $title; ?>">
<meta name="author" content="JP">
<meta name="description" content="BIG BACKEND | <?php print $title; ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BIG BACKEND | <?php print $title; ?></title>
<link href="<?php print $links['themesurl']?>css/styles.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<link href="<?php print $links['themesurl']?>js/vendors/datatables/tableTools/css/dataTables.tableTools.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/modernizr/modernizr.custom.js"></script>
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/jquery/jquery.min.js"></script>
<!--#HOOK#-->
</head>