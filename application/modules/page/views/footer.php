<!--Modals--> 

<!--Sign Out Dialog Modal-->
<div class="modal" id="signout">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <i class="fa fa-4x fa-power-off"></i> </div>
      <div class="modal-body text-center">Are You Sure Want To Sign Out?</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="yesigo">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!-- Debug Modal -->
<div class="modal" id="debug">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
          <div class="modal-body text-center">
          <div class="debug">
      <?php
      if (ENVIRONMENT == 'development')
      {
        echo '<pre>';
        print_r($this->session->userdata);
        echo '</pre>';
      }
      ?>
    </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>

<!--Scripts--> 
<!--JQuery--> 
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/jquery/jquery-ui.min.js"></script> 

<script>
var baseurl = "<?php print $links['baseurl']?>";
</script>
<script>
$('.powerwidget > header').on('touchstart', function(event){});
</script>

<!--Fullscreen--> 
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/fullscreen/screenfull.min.js"></script> 

<!--Sparkline--> 
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/sparkline/jquery.sparkline.min.js"></script> 

<!--Horizontal Dropdown
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/horisontal/cbpHorizontalSlideOutMenu.js"></script>-->  
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/classie/classie.js"></script> 

<!--Bootstrap--> 
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php print $links['themesurl']?>js/vendors/powerwidgets/powerwidgets.min.js"></script>
<!--/Scripts-->
<script type="text/javascript" src="<?php print $links['themesurl']?>js/custom/min/jquery.dataTables.min.js"></script>
<?php print $links['append_js'];?>
<!--Main App--> 
<script type="text/javascript" src="<?php print $links['themesurl']?>js/custom/scripts.js"></script>



<!-- Select2 -->
<link href="<?php print $links['themesurl']?>js/vendors/select2-3.5.1/select2.css" rel="stylesheet" type="text/css">
<script src="<?php print $links['themesurl']?>js/vendors/select2-3.5.1/select2.min.js"></script>




</body>
</html>