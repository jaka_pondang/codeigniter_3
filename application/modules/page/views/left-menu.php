<!--Main Menu-->
    <div class="responsive-admin-menu">
      <div class="responsive-menu"><span class="text-red">  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK4AAAB6CAMAAAARfZjlAAAAM1BMVEUAAAD///////////////////////////////////////////////////////////////+3leKCAAAAEHRSTlMAECAwQFBgcICPn6+/z9/vIxqCigAABAlJREFUeAHt2tGO6ygMgGFDwCFgwO//tDur0R7t5Ng0lNDhIt911f6iFAgqPB6Px+PxeDwmcRgifYsBHazL+Jj5LB9+yVZMrKjBwFpc5Jbql4olfgUXjhVUA0swkS/ZYQV75WsIfp9JfFWFX7dVvg5+G3KHAh9h9xA8SA7uQfABG6nLfOQu4aNfNw7Wsv3s5HRjtfFztcLcC9ynmk8vU/bamlDoSznXbjAd8Q/+9XpbDm/gmwuZ/yALdzHui/R2qP60TZFj8bwEEn+p0d3TGRLxf0pCAz8kNTeyoCJM4/YojFD8EcxarmcBGZjC4kH6ob+dq0+FAyTjX3+9eujftNzwiWeFDWPmk2avU3Lt9Nptp8oXVfsiN05+UMDMPWI71/btrxsGKsxcooUrLHEn28yNfJbNpd90xfHzviS2ck3lM2l/lX7T1cIrpnK32srF10vYdtC7h7LIb/CNXGoftdqTz0Cb5XcEPde+GlxXWeegbed3kJ6LLx4S3NDNCN2dm/gkw//ZOvS4Vu7Ore0BizyUyzfn2vZ0tDwzN1MKR9FyUfgw376a2SfllhicVU7hzEk+eAWA0L6aCRNyKXjT/gaDmpv4hCbnxgsvcmoutXPx7ly68KIKam5uv5+9O9df2PminstC7syFzMGJkGU7civ0bBOpN9fAH9onROjIZaOdV3MKkU+oNxfOXOET25Xr4CezJ6IYHHxxN+cazCxE9eQGaLg110dpp4NWbhZfPz/XYKzN52B13T2zM3Pd91VbYoWDZm7qmg3bDbnuKKyqvp0buu6ZaXxXq9wWTSsXe27HYvdlD/fLppG7sQAv32/YwVy5V8+Feu1i1yDpq46OB3rF3HTlLOAji3BKLpOeu7OooIFvFlNlWTVzcjmouZY1OYZwUGUdwqTcarVcIH4Xwaxcjmou8sAQzMplq+VC4fc4mJgb1FzktyDMzM1qLmTuVxHuySWizAKj5ro3ajcYzqVjdwb+ZZMw1dRcCNwpGRjJrRS8fXFTGQC1XEjco3q4Sig9cLtyDxzAqbkm83WHgcuECpHvyO3qjRbg/tzQlwsQB2LHczOfYTsXfB2IHcx10srQzgUTmsFlNwBzcjfhg0HMPQUXrfXYQDGeuwu1Sc4980fmMwpq63iuwcIClHMFxu0h0bcYvJ46nmswsqiAkLvDbK1cFzJrUMrdfit3w4O4IYOQW+DzudGFlK8doOzn/yE6dJQuPwfXLJob4BueRnzN3CAdK8sGa+ai8Lf9GgyslSuPoouZc0IDsGTuoYQtmVsc/K6uWITfNhC7ci4JsavmlsPCGgq/UqODZSRuyscGK0FWlYgGFmMqS+jwBlbk/y7FDdbl68KlAoORiMInSx+Px+PxeDwej8c/e1G5u2r3nUsAAAAASUVORK5CYII=" class="logo_top_menu" style="margin-top: 0"/>
</span>
        <div class="menuicon"><i class="fa fa-angle-down"></i></div>
      </div>
    <?php print $links['menu_bigback'];?>
    </div>
    <!--/MainMenu-->
    
    <div class="content-wrapper"> 
    <?php 
    /*
     <ul id="menu">
        <li><a class="active" href="<?php print $links['baseurl']?>dashboard" title="Dashboard"><i class="entypo-home"></i><span> Dashboard</span></a></li>
        <li><a class="submenu" href="#" title="Warehouse" data-id="warehouse-sub"><i class="entypo-key"></i><span>Warehouse</span></a>
        	<ul id="warehouse-sub">
        		<li><a href="<?php print $links['baseurl']?>warehouse/pp" title="Pick & Pack"><i class="fa fa-dropbox"></i><span>Manage Pick &amp; Pack</span></a></li>
        		<li><a href="<?php print $links['baseurl']?>warehouse/reports" title="Reports"><i class="fa fa-barcode"></i><span>Closing</span></a></li>
        		
        		<li><a href="<?php print $links['baseurl']?>warehouse/pickpack" title="Pick & Pack Reports"><i class="fa fa-send"></i><span>FLIGHT STOCK</span></a></li>
        	</ul>
        </li>
          <li><a class="submenu" href="#" title="Prebook" data-id="prebook-sub"><i class="fa fa fa-cubes"></i><span>Preorder</span></a>
              <ul id="prebook-sub">
                  <li><a href="<?php print $links['baseurl']?>prebook/mapping" title="Product Mapping"><i class="fa fa fa-sitemap"></i><span> Product Mapping</span></a></li>
                  <li><a href="<?php print $links['baseurl']?>prebook/preorder" title="Preorder"><i class="fa fa fa-cube"></i><span> Preorder Data</span></a></li>
                  <li><a href="<?php print $links['baseurl']?>prebook/preorder_print" title="Print Preorder Form"><i class="fa fa fa-print"></i><span> Print Preorder Form</span></a></li>
              </ul>
          </li>
        <li><a class="submenu" href="#" title="Cashier" data-id="cashier-sub"><i class="fa fa fa-shopping-cart"></i><span>Cashier</span></a>
        	<ul id="cashier-sub">
        		<li><a href="<?php print $links['baseurl']?>cashier/collection" title="Cash Collection"><i class="fa fa fa-money"></i><span>Cash Collection</span></a></li>
                <li><a href="<?php print $links['baseurl']?>cashier/bank_rate" title="Bank Collection Rate"><i class="fa fa fa-bank"></i><span>Bank Collection Rate</span></a></li>
        		
        		<li><a href="<?php print $links['baseurl']?>cashier/orders" title="Manual Order"><i class="fa fa fa-shopping-cart"></i><span>Manual Orders</span></a></li>
        	</ul>
        </li>
        <li><a class="submenu" href="#" title="Sales" data-id="sales-sub"><i class="entypo-doc-text"></i><span>Reports</span></a>
        	<ul id="sales-sub">
				<li><a href="<?php print $links['baseurl']?>reports/flight_stock" title="Custom Reports"><i class="glyphicon glyphicon-tree-conifer"></i><span>Custom</span></a></li>
				<li><a href="<?php print $links['baseurl']?>reports/preorder" title="Preorder Reports"><i class="glyphicon glyphicon-shopping-cart"></i><span>Preorder</span></a></li>
				<li><a href="<?php print $links['baseurl']?>reports/cashier" title="Cashier Reports"><i class="entypo-doc-text"></i><span>Cashier</span></a></li>
        		<li><a href="<?php print $links['baseurl']?>reports/commissions" title="Commissions Reports"><i class="glyphicon glyphicon-euro"></i><span>Commissions</span></a></li>
        	
        		<li><a href="<?php print $links['baseurl']?>reports/sales" title="Sales Reports"><i class="glyphicon glyphicon-usd"></i><span>SALES</span></a></li>
        			<li><a href="<?php print $links['baseurl']?>reports/decrepancy" title="Decrepancy Reports"><i class="glyphicon glyphicon-tasks"></i><span>Decrepancy</span></a></li>
        	</ul>
        </li>
         
        <li>
        <a class="submenu" href="#" title="Sales" data-id="logs-sub"><i class="glyphicon glyphicon-eye-open"></i><span>Logs</span></a>
        	<ul id="logs-sub">
        		<li><a href="<?php print $links['baseurl']?>logs/user_logs" title="Users Management"><i class="fa fa fa-users"></i><span>User Logs</span></a></li>
        		
        	</ul>
        </li>
        <li><a class="submenu" href="#" title="Users" data-id="users-sub"><i class="fa fa fa-user"></i><span>Users</span></a>
        	<ul id="users-sub">
        		<li><a href="<?php print $links['baseurl']?>users/management" title="Users Management"><i class="fa fa fa-users"></i><span>Management</span></a></li>
        		<li><a href="<?php print $links['baseurl']?>users/roles" title="Users Role"><i class="fa fa fa-mortar-board"></i><span>Roles</span></a></li>
        	</ul>
        </li>
         
        <li><a class="submenu" href="#" title="Sales" data-id="setting-sub"><i class="fa fa fa-cog"></i><span>Setting</span></a>
        	<ul id="setting-sub">
        		
        		<!--<li><a href="<?php print $links['baseurl']?>setting/currency" title="Currency"><i class="fa fa fa-money"></i><span>Currency</span></a></li>-->
                <li><a href="<?php print $links['baseurl']?>setting/flights" title="Flights"><i class="fa fa fa-plane"></i><span> Flights</span></a></li>
                <li><a href="<?php print $links['baseurl']?>setting/devices" title="Devices"><i class="fa fa fa-tablet"></i><span> Devices</span></a></li>
                <li><a href="<?php print $links['baseurl']?>setting/version" title="Version"><i class="fa fa fa-tags"></i><span>Version</span></a></li>

        		<?php if($this->session->userdata('user_id')==0){?>
        			<li><a href="<?php print $links['baseurl']?>setting/clean" title="Currency"><i class="fa fa fa-trash-o"></i><span>CleaN</span></a></li>
        		<?php }?>
        	</ul>
        </li>
      </ul>
    */
    ?>
        