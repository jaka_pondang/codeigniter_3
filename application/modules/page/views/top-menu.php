
<!--Smooth Scroll-->
<div class="smooth-overflow">
<!--Navigation-->
  <nav class="main-header clearfix" role="navigation"> <a class="navbar-brand" href="<?php print $links['baseurl']?>dashboard"><span class="text-red">
  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK4AAAB6CAMAAAARfZjlAAAAM1BMVEUAAAD///////////////////////////////////////////////////////////////+3leKCAAAAEHRSTlMAECAwQFBgcICPn6+/z9/vIxqCigAABAlJREFUeAHt2tGO6ygMgGFDwCFgwO//tDur0R7t5Ng0lNDhIt911f6iFAgqPB6Px+PxeDwmcRgifYsBHazL+Jj5LB9+yVZMrKjBwFpc5Jbql4olfgUXjhVUA0swkS/ZYQV75WsIfp9JfFWFX7dVvg5+G3KHAh9h9xA8SA7uQfABG6nLfOQu4aNfNw7Wsv3s5HRjtfFztcLcC9ynmk8vU/bamlDoSznXbjAd8Q/+9XpbDm/gmwuZ/yALdzHui/R2qP60TZFj8bwEEn+p0d3TGRLxf0pCAz8kNTeyoCJM4/YojFD8EcxarmcBGZjC4kH6ob+dq0+FAyTjX3+9eujftNzwiWeFDWPmk2avU3Lt9Nptp8oXVfsiN05+UMDMPWI71/btrxsGKsxcooUrLHEn28yNfJbNpd90xfHzviS2ck3lM2l/lX7T1cIrpnK32srF10vYdtC7h7LIb/CNXGoftdqTz0Cb5XcEPde+GlxXWeegbed3kJ6LLx4S3NDNCN2dm/gkw//ZOvS4Vu7Ore0BizyUyzfn2vZ0tDwzN1MKR9FyUfgw376a2SfllhicVU7hzEk+eAWA0L6aCRNyKXjT/gaDmpv4hCbnxgsvcmoutXPx7ly68KIKam5uv5+9O9df2PminstC7syFzMGJkGU7civ0bBOpN9fAH9onROjIZaOdV3MKkU+oNxfOXOET25Xr4CezJ6IYHHxxN+cazCxE9eQGaLg110dpp4NWbhZfPz/XYKzN52B13T2zM3Pd91VbYoWDZm7qmg3bDbnuKKyqvp0buu6ZaXxXq9wWTSsXe27HYvdlD/fLppG7sQAv32/YwVy5V8+Feu1i1yDpq46OB3rF3HTlLOAji3BKLpOeu7OooIFvFlNlWTVzcjmouZY1OYZwUGUdwqTcarVcIH4Xwaxcjmou8sAQzMplq+VC4fc4mJgb1FzktyDMzM1qLmTuVxHuySWizAKj5ro3ajcYzqVjdwb+ZZMw1dRcCNwpGRjJrRS8fXFTGQC1XEjco3q4Sig9cLtyDxzAqbkm83WHgcuECpHvyO3qjRbg/tzQlwsQB2LHczOfYTsXfB2IHcx10srQzgUTmsFlNwBzcjfhg0HMPQUXrfXYQDGeuwu1Sc4980fmMwpq63iuwcIClHMFxu0h0bcYvJ46nmswsqiAkLvDbK1cFzJrUMrdfit3w4O4IYOQW+DzudGFlK8doOzn/yE6dJQuPwfXLJob4BueRnzN3CAdK8sGa+ai8Lf9GgyslSuPoouZc0IDsGTuoYQtmVsc/K6uWITfNhC7ci4JsavmlsPCGgq/UqODZSRuyscGK0FWlYgGFmMqS+jwBlbk/y7FDdbl68KlAoORiMInSx+Px+PxeDwej8c/e1G5u2r3nUsAAAAASUVORK5CYII=" class="logo_top_menu"/>
  </span></a>
    
    <!--Search
    <div class="site-search">
      <form action="#" id="inline-search">
        <i class="fa fa-search"></i>
        <input type="search" placeholder="Search">
      </form>
    </div>
    -->
    <!--Navigation Itself-->
    
    <div class="navbar-content"> 
      <div class="pull-right">
      <!--Sidebar Toggler
      <a href="#" class="left-toggler"><i class="fa fa-bars"></i></a> --> 
      
      <!--Fullscreen Trigger-->
      <a href="#" class="hidden-xs" id="toggle-fullscreen"> <i class=" entypo-popup"></i> </a>
      
      <!--Lock Screen--> 
      <a href="#" data-toggle="modal" class="signout"> <i class="fa fa-power-off"></i> </a> 
      </div>
      <small class="text-white select-store">SELECT STORE <i class="top-chevron fa fa-angle-double-right"></i></small>
         <form method="POST" action="<?php print $links['baseurl'];?>collection" class="form-inline collection-selector">
           <?php if(isset($function)&&!empty($function)){
              $url_function="";//"/".$function;
           }else{
             $url_function = "";
           }?>
           <input type="hidden" name="urlback" value="<?php echo base_url().uri_string(); ?><?php echo ($_SERVER['QUERY_STRING'] != "") ? "?".$_SERVER['QUERY_STRING'] : ""?>" />
            <select id="collection-selector" class="form-control" name="collection_id" onchange="this.form.submit()" disabled>
              <option>Select Collection</option>
              <?php 
                foreach($collections as $collection)
                {
                  if($collection->collection_id == $collection_assigned)
                  {
                    ?>
                    <option selected value="<?php echo $collection->collection_id?>"><?php echo $collection->collection_name; ?></option>
                    <?php
                  }else
                  {
                    ?>
                    <option value="<?php echo $collection->collection_id?>"><?php echo $collection->collection_name; ?></option>
                    <?php
                  }
                }
              ?>
            </select>
          </form>
      <?php
      if (ENVIRONMENT == 'development')
      {
        ?>
        <div class="pull-right" data-toggle="modal" data-target="#debug">
            <h5 href="#" style="color: white;">Debug &nbsp;</h5>
        </div>
        <?php
      }
      ?>
      
      <!--Notifications Dropdown
      
      <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="entypo-megaphone"></i><span class="new"></span></button>
        <div id="notification-dropdown" class="dropdown-menu">
          <div class="dropdown-header">Notifications <span class="badge pull-right">8</span></div>
          <div class="dropdown-container">
            <div class="nano">
              <div class="nano-content">
                <ul class="notification-dropdown">
                  <li class="bg-warning"><a href="#"> <span class="notification-icon"><i class="fa fa-bolt"></i></span>
                    <h4>Server Down</h4>
                    <p>Server #435 was shut down (Power loss)</p>
                    <span class="label label-default"><i class="entypo-clock"></i> 59 mins ago</span> </a> </li>
                  <li class="bg-info"><a href="#"> <span class="notification-icon"><i class="fa fa-bolt"></i></span>
                    <h4>Too Many Connections</h4>
                    <p>Too many connections to Database Server</p>
                    <span class="label label-default"><i class="entypo-clock"></i> 2 hours ago</span> </a> </li>
                  <li class="bg-danger"><a href="#"> <span class="notification-icon"><i class="fa fa-android"></i></span>
                    <h4>Sausage Stolen</h4>
                    <p>Someone stole your hot sausage</p>
                    <span class="label label-default"><i class="entypo-clock"></i> 3 hours ago</span> </a> </li>
                  <li class="bg-success"><a href="#"> <span class="notification-icon"><i class="fa fa-bolt"></i></span>
                    <h4>Defragmentation Completed</h4>
                    <p>Disc Defragmentation Completed on Server</p>
                    <span class="label label-default"><i class="entypo-clock"></i> 3 hours ago</span> </a> </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="dropdown-footer"><a class="btn btn-dark" href="#">See All</a></div>
        </div>
      </div>
      -->
      <!--Inbox Dropdown
      <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="entypo-mail"></i><span class="new"></span></button>
        <div id="inbox-dropdown" class="dropdown-menu inbox">
          <div class="dropdown-header">Inbox <span class="badge pull-right">32</span></div>
          <div class="dropdown-container">
            <div class="nano">
              <div class="nano-content">
                <ul class="inbox-dropdown">
                  <li><a href="#"> <span class="user-image"><img src="http://placehold.it/150x150" alt="Gluck Dorris" /></span>
                    <h4>Why don't u answer calls?</h4>
                    <p>Listen, dude, time is off. I'll find you soon! Sounds good?...</p>
                    <span class="label label-default"><i class="entypo-clock"></i> 59 mins ago</span> <span class="delete"><i class="entypo-back"></i></span> </a> </li>
                  <li><a href="#"> <span class="user-image"><img src="http://placehold.it/150x150" alt="Gluck Dorris" /></span>
                    <h4>Rawrr, rawrrr...</h4>
                    <p>Listen, dude, time is off. I'll find you soon! Sounds good?...</p>
                    <span class="label label-default"><i class="entypo-clock"></i> 2 hours ago</span> <span class="delete"><i class="entypo-back"></i></span> </a> </li>
                  <li><a href="#"> <span class="user-image"><img src="http://placehold.it/150x150" alt="Gluck Dorris" /></span>
                    <h4>Why so serious?</h4>
                    <p>Listen, dude, time is off. I'll find you soon! Sounds good?...</p>
                    <span class="label label-default"><i class="entypo-clock"></i> 3 hours ago</span> <span class="delete"><i class="entypo-back"></i></span> </a> </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="dropdown-footer"><a class="btn btn-dark" href="admin-inbox.html">Save All</a></div>
        </div>
      </div>-->
    </div>
  </nav>
  
  <!--/Navigation--> 
    
  <!--MainWrapper-->
  <div class="main-wrap"> 
  	
        